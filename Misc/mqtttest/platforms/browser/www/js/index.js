/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
            // Amazon link (Required)
            AWS.config.update({accessKeyId: 'AKIAJIKYUQBYL6RLOFJA', secretAccessKey: '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'}); 
            try{
                    console.log("Inside try for AWS connect");
                    // var dynamodb = new AWS.DynamoDB({region: 'us-west-2'});
                    // List your table
                    //dynamodb.describeTable(params, function(err, data) {
                    //    if (err)
                    //        console.log(JSON.stringify(err, null, 2));
                    //    else
                    //        console.log(JSON.stringify(data, null, 2));
                    //});
                    //
                    // List your table
                    // dynamodb.listTables(function(err, data) {
                    //   console.log(data.TableNames);
                    // });

                    // Get an item from a table
                    var docClient = new AWS.DynamoDB.DocumentClient({region: 'us-west-2'});
                     
                    //var params = {
                    //    Key: {
                    //        "TST": '0001',
                    //    },
                    //    TableName: 'ownMQTT'
                    //};
                    // 
                    //docClient.get(params, function(err, data){
                    //    if (err) console.log(err);
                    //    else console.log(data); 
                    //});
                    // Write the item to the table
                    //var params = {
                    //    TableName: "ownMQTT",
                    //    Item: {
                    //        "TST":"0002",
                    //        "LAT": 100,
                    //        "LON": 200
                    //    }
                    //};

                    //docClient.put(params, function(err, data) {
                    //    if (err)
                    //        console.log(JSON.stringify(err, null, 2));
                    //    else
                    //        console.log(JSON.stringify(data, null, 2));
                    //});
                    console.log('AWS access Worked');
        }catch(e){
            console.log('Not Working');
            console.log(e);
        }
        // Create a client instance
            client = new Paho.MQTT.Client("m10.cloudmqtt.com", 32230,"webBrowser"); 
            //Example client = new Paho.MQTT.Client("m11.cloudmqtt.com", 32903, "web_" + parseInt(Math.random() * 100, 10));

          // set callback handlers
          client.onConnectionLost = app.onConnectionLost;
          client.onMessageArrived = app.onMessageArrived;
          var options = {
            useSSL: true,
            userName: "fhrthprh",
            password: "Bj7hVH1gYadq",
            onSuccess:app.onConnect,
            onFailure:app.doFail
        };
        try{
          console.log("Inside try for MQTT connect");

        //// connect the client
        client.connect(options);
        }catch(e)
        {
            console.log(e);
        }
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    // called when the client connects
    onConnect: function() {
    // Once a connection has been made, make a subscription and send a message.
        console.log("onConnect");
        client.subscribe("owntracks/fhrthprh/#");
        message = new Paho.MQTT.Message("Hello CloudMQTT");
        message.destinationName = "/cloudmqtt";
        client.send(message); 
  },
   doFail: function(e){
    console.log(e);
  },
  // called when the client loses its connection
   onConnectionLost: function (responseObject) {
    if (responseObject.errorCode !== 0) {
      console.log("onConnectionLost:"+responseObject.errorMessage);
    }
  },
// called when a message arrives
  onMessageArrived: function(message) {
    console.log("onMessageArrived:"+message.payloadString);
    var element = document.getElementById('geolocation');
    res=JSON.parse(message.payloadString);
    element.innerHTML = 'TrackID:' +  res.tid + '<br />' +'TST:' +  res.tst + '<br />' +'LAT:' +  res.lat + '<br />' + 'LONG:' +  res.lon+ '<br />' ; 
    console.log("onMessageArrived:"+res.tid);
    console.log("onMessageArrived:"+res.lat);
    console.log("onMessageArrived:"+res.lon);
    console.log("onMessageArrived:"+res.tst);
  }
};
