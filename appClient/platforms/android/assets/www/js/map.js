// A function that is called by db_init() in amazonDBInterface.js to plot the
// markers
function CreateWaypoints(data) 
{
    // data - information from Amazon DynamoDB queried for the particular device
    console.log("inCreateWaypoints");

    var GPSCoordinates = [];
    // To ensure all markers are visible by the zoom level, we use markerBounds
    var markerBounds = new google.maps.LatLngBounds();
    if(data.Count==0)     // If no data
    {
        var mapdiv = document.getElementById("map");
        mapdiv.innerHTML = "Database is empty";
    }
    else
    {
        console.log("initializing map");
        var mapOptions={
            center: {lat: -35.090, lng: 105.712},
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var itemlist = data.Items;
        var dev=itemlist[0].TID;
        var radiusMarker=0;
        // Hardcoding my marker sizes based on the database information
        if (dev=="le")
        {
            radiusMarker=300;
        }
        else
        {
            radiusMarker=10;
        }
        console.log("Drawing points at radius:"+radiusMarker.toString());
        data.Items.forEach(function(res) 
        {
            console.log("{"+ res.LAT + ", " + res.LON + "}");
            markerBounds.extend(new google.maps.LatLng(res.LAT, res.LON));
            
            // For circle
            var circ = new google.maps.Circle({
                center: {lat: res.LAT, lng: res.LON} ,
                clickable:false,
                fillColor:"#00FF00",
                fillOpacity:1, 
                map: map,
                radius:radiusMarker, 
                strokeColor:'#0000FF',
                strokeOpacity:'0.5'
            });
        });
        // Ensure all markers are seen
        map.fitBounds(markerBounds);
        console.log("Map drawn");
    }
}
