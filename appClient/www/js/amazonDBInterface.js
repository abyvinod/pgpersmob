function db_init(parsedInfo,callback)
{
    console.log("inUpdateMQTTdynamoDB");
    // Amazon credentials, region. Disable CRC32 check so that huge data can be
    // queried.
    AWS.config.update({dynamoDbCrc32: false, accessKeyId:
        'AKIAJIKYUQBYL6RLOFJA', secretAccessKey:
        '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', region: "us-west-2"});
    // Query the data based on the device and execute the callback function
    getData(parsedInfo,callback);
} 

function getData(parsedInfo,dataprocessCallback)
{
    console.log("Trying to get data");

    var docClient = new AWS.DynamoDB.DocumentClient();
    
    console.log("Querying for registrations with mobile");
    
    // device name is in the parsedInfo
    var params = {
        TableName : "oMQTT",
        KeyConditionExpression: "TID = :device",
        ExpressionAttributeValues: 
        { 
            ":device":parsedInfo[0]
        }
    };

    docClient.query(params, function(err, data) {
        if (err) {
            console.log(err);
            console.log("Unable to query. Error:", JSON.stringify(err, null, 2));
        } else {
            console.log("Query succeeded.");
            dataprocessCallback(data);  // Either AddRow or CreateWaypoint (see map.js)
        }
    });
}

function AddRow(data) 
{
    tableId="GPSTable";
    console.log("inAddRow");
    var table = document.getElementById(tableId);
    if (table == undefined) 
        return;
    // Create an empty <tr> element and add it to the 1st position of the table:
    var row,cell1,cell2,cell3,cell4;
    if(data.Count==0)
    {
        var tablediv = document.getElementById("form");
        tablediv.innerHTML = "Table is empty";
    }
    else
    {
        data.Items.forEach(function(res) 
        {
            console.log(">"+ res.TST + ": " + res.TID);

            row = table.insertRow(1);
        
            // Insert new cells (<td> elements) at the 1st and 2nd position of the
            // "new" <tr> element:
            cell1 = row.insertCell(0);
            cell2 = row.insertCell(1);
            cell3 = row.insertCell(2);
            cell4 = row.insertCell(3);
        
            // Add some text to the new cells:
            var date = new Date(0);
            date.setUTCSeconds(res.TST); 
            cell1.innerHTML = res.TID;
            cell2.innerHTML = date.getDate()+"/"+date.getMonth()+"/"+date.getFullYear()+", "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
            cell3.innerHTML = Math.round(res.LAT*100000)/100000;
            cell4.innerHTML = Math.round(res.LON*100000)/100000; 
        });
    }
}

function updateInst2DB()
{
    console.log("inUpdateInstrumentation");
    // Amazon link (Required)
    AWS.config.update({dynamoDbCrc32: false, accessKeyId: 'AKIAJIKYUQBYL6RLOFJA', secretAccessKey: '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'}); 
    var docClient = new AWS.DynamoDB.DocumentClient({region: 'us-west-2'});
     
    // Write the item to the table
    var params = {
        TableName: "PMVinst",
        Item: {
            "UID": device.uuid,
            "TST": Math.round(new Date().getTime()/1000),
            "IND": instApp[0],
            "TAB": instApp[1],
            "MAP": instApp[2],
            "SUM": instApp[0]+instApp[1]+instApp[2]
        }
    };

    try{
            console.log("Trying to put in the item");
            console.log(params.Item);
            docClient.put(params, function(err, data) {
                if (err)
                    console.log(JSON.stringify(err, null, 2));
                else
                {
                    console.log(JSON.stringify(data, null, 2));
                    console.log('AWS access Worked');
                }
            });
    }catch(e){
        console.log('Not Working');
        console.log(e);
    }
}
// DynamoDB code
// var dynamodb = new AWS.DynamoDB({region: 'us-west-2'});
// List your table
//dynamodb.describeTable(params, function(err, data) {
//    if (err)
//        console.log(JSON.stringify(err, null, 2));
//    else
//        console.log(JSON.stringify(data, null, 2));
//});
//
// List your table
// dynamodb.listTables(function(err, data) {
//   console.log(data.TableNames);
// });
// Get an item from a table
//var params = {
//    Key: {
//        "TST": '0001',
//    },
//    TableName: 'ownMQTT'
//};
// 
//docClient.get(params, function(err, data){
//    if (err) console.log(err);
//    else console.log(data); 
//});
// Put an item into the table
//var docClient = new AWS.DynamoDB.DocumentClient({region: 'us-west-2'});
// 
//// Write the item to the table
//var params = {
//    TableName: "ownMQTT",
//    Item: {
//        "TST": res.tst,
//        "TID": res.tid,
//        "LAT": res.lat,
//        "LON": res.lon
//    }
//};
//
//docClient.put(params, function(err, data) {
//    if (err)
//        console.log(JSON.stringify(err, null, 2));
//    else
//        console.log(JSON.stringify(data, null, 2));
//});

