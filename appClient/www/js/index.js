var instFreq=1000;                  // In milliseconds
var instApp;                        // Array 1x3 containing the page activity
                                    // information for [index, table, map].html
var instUpdateTime=(instFreq/1000); // Computes the increments in instApp in seconds

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        // Defined in each of the htmls
        page_init();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

// Back Button Navigation
var stopCounting=0;
function backbuttonfunction()
{
    console.log(pageNUM);
    if(pageNUM==0)
    {
        // Exit if at index.html
        stopCounting=1;
        console.log("At home. Exiting");
        navigator.notification.confirm(
        'Leaving your Personal Mobility Visualizer?', // message
        exitapp,
        'Confirm exit',           // title
        ['Yes','No']);         // buttonLabels
    }
    else
    {
        // Else, go to index.html
        console.log("Going to home.");
        window.sessionStorage.setItem("instAppInfo", JSON.stringify(instApp));
        window.location.replace("index.html");
    }
}

function exitapp(buttonIndex)
{
    console.log(buttonIndex);
    if(buttonIndex==1)
    {
        // function in amazonDBInterface.js for updating the database with
        // instrumentation information
        updateInst2DB();
        navigator.app.exitApp();
    }
    else
    {
        // The user changed his mind. Continue with the instrumentation.
        stopCounting=0;
    }
}

function instrumentApp()
{
    var timeInfo = window.sessionStorage.getItem("instAppInfo");
    if(timeInfo==null)
    {
        // Initialize the instApp vector
        console.log("Setting times to zero");
        instApp=[0,0,0];
    }
    else
    {
        // Update the instApp vector with existing information
        instApp=JSON.parse(timeInfo);
        console.log("Obtained instrumentation times");
        console.log(instApp);
    }
    
    // update time info based on pageNUM
    updateInstApp();
}

function updateInstApp()
{
    var pageSTR=["index.html","table.html","map.html"];
    if(stopCounting==0)
    {
        // Based on instUpdateTime (defined in line 3 of this js file), update
        // timers
        instApp[pageNUM]=instApp[pageNUM]+instUpdateTime;
    }
    console.log("At page: "+pageSTR[pageNUM]);
    console.log(instApp);
    // Call this instrumentation function after instFreq milliseconds
    setTimeout(updateInstApp,instFreq);
}

