var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        // Create a client instance for MQTT
        client = new Paho.MQTT.Client("m10.cloudmqtt.com", 32230,"webBrowser"); 

        // set callback handlers
        client.onConnectionLost = app.onConnectionLost;
        client.onMessageArrived = app.onMessageArrived;
        var options = {
            useSSL: true,
            userName: "fhrthprh",
            password: "Bj7hVH1gYadq",
            onSuccess:app.onConnect,
            onFailure:app.doFail
        };
        try{
            console.log("Inside try for MQTT connect");
            // connect the client
            client.connect(options);
        }catch(e)
        {
            console.log("Not working MQTT connect");
            console.log(e);
        }
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    // called when the client connects
    onConnect: function() {
        // Once a connection has been made, make a subscription and send a message.
        console.log("onConnect");
        client.subscribe("owntracks/fhrthprh/#");
        message = new Paho.MQTT.Message("Logger is on");
        message.destinationName = "/cloudmqtt";
        client.send(message); 
    },
    doFail: function(e){
        console.log("Not working MQTT connect:doFail");
        console.log(e);
    },
    // called when the client loses its connection
    onConnectionLost: function (responseObject) {
        if (responseObject.errorCode !== 0) {
          console.log("onConnectionLost:"+responseObject.errorMessage);
        }
    },
    // called when a message arrives
    onMessageArrived: function(message) {
        console.log("onMessageArrived:"+message.payloadString);
        res=JSON.parse(message.payloadString);
        console.log("onMessageArrived:"+res.tid);
        console.log("onMessageArrived:"+res.lat);
        console.log("onMessageArrived:"+res.lon);
        console.log("onMessageArrived:"+res.tst);
        // Add a row to the table
        app.AddRow("GPSTable",res);
        // Update the DB
        app.UpdateMQTTdynamoDB(res);
    },
    AddRow: function(tableId,res) {
        console.log("inAddRow");
        var table = document.getElementById(tableId);
        if (table == undefined) 
            return;
        // Create an empty <tr> element and add it to the 1st position of the table:
        var row = table.insertRow(1);
        
        // Insert new cells (<td> elements) at the 1st and 2nd position of the
        // "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        
        // Add some text to the new cells:
        var date = new Date(0);
        date.setUTCSeconds(res.tst); 
        cell1.innerHTML = res.tid;
        cell2.innerHTML = date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear()+","+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
        cell3.innerHTML = Math.round(res.lat*100000)/100000;
        cell4.innerHTML = Math.round(res.lon*100000)/100000; 
    },
    UpdateMQTTdynamoDB: function(res) {
        console.log("inUpdateMQTTdynamoDB");
        // Amazon link (Required)
        AWS.config.update({accessKeyId: 'AKIAJIKYUQBYL6RLOFJA', secretAccessKey:
            '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'}); 
        try{
                console.log("Inside try for AWS connect");
                var docClient = new AWS.DynamoDB.DocumentClient({region: 'us-west-2'});
                 
                // Write the item to the table
                var params = {
                    TableName: "oMQTT",
                    Item: {
                        "TID": res.tid,
                        "TST": res.tst,
                        "LAT": res.lat,
                        "LON": res.lon
                    }
                };

                docClient.put(params, function(err, data) {
                    if (err)
                        console.log(JSON.stringify(err, null, 2));
                    else
                        console.log(JSON.stringify(data, null, 2));
                });
                console.log('AWS access Worked');
        }catch(e){
            console.log('AWS update is not working');
            console.log(e);
        }
    }
};

