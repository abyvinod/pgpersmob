# Personal Mobility Visualization



**Problem**: Make a hybrid app for visualization of personal mobility data in a PhoneGap framework.

**Solution approach**: The app uses cloudMQTT to serve as the private MQTT broker and ownTracks as the data gatherer. For data storage, Amazon DynamoDB will be used and a PhoneGap application which can serve as a visualization tool for personal mobility data will be the end-result of this project. Instrumentation of the app will also be done. \[[Youtube video](https://youtu.be/gw4VlmQgx54)\] demonstrating the developed application.
![Flowchart](./Flowchart.png)



## Milestones Achieved

* Write a basic app in PhoneGap to access the GPS information and use MQTT protocols. (Checkout the projects in Misc/TestApp and Misc/mqtttest folders)
* Develop a Phonegap app (logServer) which subscribes to the cloudMQTT broker and updates the Amazon DynamoDB.
* Develop a Phonegap app (appClient) which takes input from the user about the device and the visualization technique interested. 
* Device options: "Mobile" data and "iPad" data. 
	* Visualization technique: "Map" and "Table".
	* Perform the instrumentation of the app using a custom solution based on setTimeout() and JSON. On closing the app, the instrumentation information is pushed to the database PMVinst (Amazon DynamoDB database)
* Upload the app demonstration video on [Youtube](https://youtu.be/gw4VlmQgx54).